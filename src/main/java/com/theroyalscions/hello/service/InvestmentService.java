package com.theroyalscions.hello.service;

import com.theroyalscions.hello.persistence.Investment;
import com.theroyalscions.hello.persistence.InvestmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvestmentService {
    @Autowired
    private InvestmentRepository investmentRepository;

    @Value("${hello-investment.interest-rate}")
    private double interestRate;

    public Iterable<Investment> getAll(){
        return investmentRepository.findAll();
    }

    public Investment create(Investment investment){
        calculateAmount(investment);
        return investmentRepository.save(investment);
    }

    private void calculateAmount(Investment investment) {
        investment.setAmount(investment.getValue() * Math.pow(1 + interestRate, investment.getNumberOfMonths()));
    }
}
