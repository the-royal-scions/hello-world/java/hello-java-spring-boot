package com.theroyalscions.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloJavaSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloJavaSpringBootApplication.class, args);
	}

}
