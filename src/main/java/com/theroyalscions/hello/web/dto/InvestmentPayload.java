package com.theroyalscions.hello.web.dto;

import com.theroyalscions.hello.persistence.Investment;

public class InvestmentPayload {
    private double value;
    private short numberOfMonths;
    private double amount;

    //region Get/Set
    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public short getNumberOfMonths() {
        return numberOfMonths;
    }

    public void setNumberOfMonths(short numberOfMonths) {
        this.numberOfMonths = numberOfMonths;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    //endregion

    public InvestmentPayload() {
    }

    public InvestmentPayload(Investment investment) {
        setValue(investment.getValue());
        setNumberOfMonths(investment.getNumberOfMonths());
        setAmount(investment.getAmount());
    }

    public Investment buildEntity() {
        Investment investment = new Investment();
        investment.setValue(getValue());
        investment.setNumberOfMonths(getNumberOfMonths());
        investment.setAmount(getAmount());
        return investment;
    }
}
