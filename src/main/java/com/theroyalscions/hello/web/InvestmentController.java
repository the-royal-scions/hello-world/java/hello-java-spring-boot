package com.theroyalscions.hello.web;

import com.theroyalscions.hello.persistence.Investment;
import com.theroyalscions.hello.service.InvestmentService;
import com.theroyalscions.hello.web.dto.InvestmentPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class InvestmentController {

    @Autowired
    private InvestmentService investmentService;

    //Get investment
    @GetMapping("investment")
    public List<InvestmentPayload> getAllInvestments(){
        List<InvestmentPayload> responses = new ArrayList<>();
        for (Investment investment : investmentService.getAll())
            responses.add(new InvestmentPayload(investment));
        return responses;
    }

    //Post investment
    @PostMapping("investment")
    public InvestmentPayload createInvestment(@RequestBody InvestmentPayload investmentPayload){
        Investment investment = investmentPayload.buildEntity();
        return new InvestmentPayload(investmentService.create(investment));
    }
}
