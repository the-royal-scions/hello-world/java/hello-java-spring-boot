package com.theroyalscions.hello.persistence;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InvestmentRepository extends CrudRepository<Investment, Integer> {
}
